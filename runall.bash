#!bash

for FILE in addrgroup_csv_zz; do
	postfix=`echo $FILE | grep -o ..$`
	echo $FILE $postfix
	node ./app_compare_addresses.js $FILE $postfix
done

exit 0
