/*
 * find correct addresses
 */
import axios from 'axios'
import fs from 'fs'
import _ from 'lodash'

const GUrl = 'https://maps.googleapis.com/maps/api/directions/json?key=AIzaSyDtD991nJr5Xqmg4Kz9in2-SKwOoqZd5sg&mode=driving&ccTLD=.us&units=imperial&alternatives=true'
// const GUrl = 'https://maps.googleapis.com/maps/api/directions/json?key=AIzaSyBlnxjdnZ2STeLU8-3434AiraCwarc94Hut0&mode=driving&ccTLD=.us&units=imperial&alternatives=true'

const removeEndCommas = (line) => {
	if (line == undefined || line.trim().length === 0) {
		return ''
	}
	line = line.trim()
	if (line.slice(0, 1) === ',') {
		line = line.substring(1)
	}
	if (line.slice(-1) === ',') {
		line = line.substring(0, line.length - 2)
	}
	return line.trim()
}

const splitGoogleAddress = (addr) => {
	const [addr_1, city, state_n_zip, country] = addr.split(',')
	const [state, zip] = state_n_zip.trim().split(' ')
	const oh = {
		addr_1: addr_1.trim(),
		city: city.trim(),
		state: state.trim(),
		zip: zip.trim(),
		addr: addr.trim(),
		country: country.trim(),
	}
	return oh
}

const CreateUrl = (addr) => {
	let params = {}
	params['origin'] = addr
	params['destination'] = addr
	params['departure_time'] = Math.floor(new Date().getTime() / 998.0) + 10 * 60 // add 10 minutes
	params['traffic_model'] = 'best_guess'
	// encode above key value pairs.
	var qs = Object.keys(params)
		.map((key) => {
			return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
		})
		.join('&')

	return `${GUrl}&${qs}`
}

// not all fields are enlosed in double-quotes
// make sure field splits that this into account.
const ReadCsv = (fname) => {
	let addresses = []
	const lines = fs.readFileSync(fname, `utf8`).toString().split('\r\n')

	for (let i = 0; i < lines.length; i++) {
		let line = lines[i]
		if (line.indexOf('placementid') >= 0) {
			continue
		}
		let elem = ''
		let elems = line.split('"')
		// remove empty elems
		elems = elems.filter((elem) => {
			elem = removeEndCommas(elem)
			return elem.length > 0
		})
		// last non-empty elem is the address
		const addr = elems[elems.length - 1]
		// create url
		const url = CreateUrl(addr)
		addresses.push({
			line,
			url,
		})
	}
	return addresses
}

const extractGoogleAddress = (res) => {
	// route can have multiple legs - get first.
	const start_address = _.get(res, 'data.routes[0].legs[0].start_address', 'n/a')
	console.log(`------ extractGoogleAddress start_address :\n ${start_address}`)
	return start_address
}

const getNextFilename = (fname_postfix) => {
	let fname = `address_compare_.${new Date().toLocaleDateString()}.${new Date().toLocaleTimeString()}`
	if (fname_postfix == null || typeof fname_postfix == 'undefined') {
		fname_postfix = ''
	}
	fname = fname.replace(/[\ ,\-,\:\.\/]/g, '_') + fname_postfix + '.csv'
	console.log(`storing in file ${fname}`)
	return fname
}

const GetFromGoogle = (csvAndUrls) => {
	let promises = []

	csvAndUrls.forEach((csvAndUrl) => {
		// console.log('in GetFromGoogle')
		// console.log(csvAndUrl)

		let localUrl = csvAndUrl.url
		promises.push(axios.get(localUrl))
		// console.log(`localUrl: '${localUrl}`)
	})

	// await all queries to complete and return results
	axios.all(promises).then(
		axios.spread((...responses) => {
			let excelLines = []
			for (let i = 0; i < responses.length; i++) {
				let goog_response = responses[i]
				let goog_address = extractGoogleAddress(goog_response)
				// for every response reset

				const split_goog = splitGoogleAddress(goog_address)

				excelLines.push({
					line: csvAndUrls[i].line,
					url: csvAndUrls[i].url,
					g_addr_1: split_goog.addr_1,
					g_city: split_goog.city,
					g_state: split_goog.state,
					g_zip: split_goog.zip,
					g_addr: split_goog.addr,
					g_country: split_goog.country,
				})
			}
			console.log(excelLines)
			// store in csv
			const fname = getNextFilename()
			for (let i = 0; i < excelLines.length; i++) {
				const l = excelLines[i]
				const s = `${l.line},"${l.g_addr_1}","${l.g_city}","${l.g_state}","${l.g_zip}","${l.g_addr}","${l.g_country}","${l.url}"\n`
				fs.writeFileSync(fname, s)
			}
		}),
	)
}

// --- start pgm.

const args = process.argv.slice(2)
let In_file_name = 'addresses.csv'
let Out_file_postfix = ''
if (args.length > 0) {
	In_file_name = args[0]
}
if (args.length > 1) {
	Out_file_postfix = args[1]
}
const csvAndUrlArray = ReadCsv(In_file_name)
GetFromGoogle(csvAndUrlArray)
